![picture](https://i.imgur.com/eSpsPCF.png)

Unofficial Tailscale desktop application, Open source and multi-platform for all platforms to use. mananage your tailscale network, devices and more from the application.

&nbsp;&nbsp;&nbsp;&nbsp;

![picture](https://i.imgur.com/j6T2Qoq.png)

&nbsp;&nbsp;&nbsp;&nbsp;

  You can install Tailscale from the AUR for Arch/Manjaro distros.
 [Click Here](https://aur.archlinux.org/packages/tailscaledesktop)

 ### Download For All platforms (Linux, Mac OS and Windows)
  
  [Click to get the latest release](https://gitlab.com/tailscaledesktop/application/-/releases)

 ### Author
  * Corey Bruce